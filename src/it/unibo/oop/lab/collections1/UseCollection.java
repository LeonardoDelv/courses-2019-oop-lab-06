package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {
	
	private static final int MIN = 1000;
	private static final int MAX = 2000;
	private static final int ELEMS = 100000;
	private static final int READS = 10000;
	
    private static final int TO_MS = 1_000_000;
    
    private static final long AFRICA = 1_110_635_000L;
    private static final long AMERICAS = 972_005_000L;
    private static final long ANTARCTICA = 0;
    private static final long ASIA = 4_298_723_000L;
    private static final long EUROPE = 742_452_000L;
    private static final long OCEANIA = 38_304_000L;
	
    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	
    	final ArrayList<Integer> list1 = new ArrayList<>();
    	
    	for(int i = UseCollection.MIN; i < UseCollection.MAX; i ++) {
    		list1.add(i);
    	}
    	System.out.println("Elementi list1: " +list1);
    	
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	
    	final LinkedList<Integer> list2 = new LinkedList<>(list1);
    	System.out.println("Elementi list2: " +list2);
    	
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	
    	final int listTemp = list1.get(list1.size() - 1); //ultimo elemento messo in temp
    	list1.set(list1.size() - 1, list1.get(0)); //metto nell'ultima posizione il primo elemento
    	list1.set(0,  listTemp);	//metto in prima posizione l'ultimo elemento che era in temp
    	
    	System.out.println(list1);
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	
    	for(final int a : list1) {
    		System.out.print(a);
    		System.out.print(", ");
    	}
    	System.out.println();
    	
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	
    	//ArrayList
    	long time = System.nanoTime();
    	for(int i = 0; i < ELEMS; i ++) {
    		list1.add(0, i);
    	}
    	
    	time = System.nanoTime() - time;
    	System.out.println("Inserting " + ELEMS
                + " as first in list1 tooks " + time
                + "ns (" + time / TO_MS + "ms)");
    	
    	//LinkedList
    	time = System.nanoTime();
    	for(int i = 0; i < ELEMS; i ++) {
    		list2.add(0, i);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Inserting " + ELEMS
                + " as first in list2 tooks " + time
                + "ns (" + time / TO_MS + "ms)");
    	
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	
    	//ArrayList
    	time = System.nanoTime();
    	for(int i = 0; i < READS; i ++) {
    		list1.get(list1.size() / 2);
    	}
    	
    	time = System.nanoTime() - time;
    	System.out.println("Reading " + READS
                + " times the middle element of list1 " + time
                + "ns (" + time / TO_MS + "ms)");
    	
    	//LinkedList
    	time = System.nanoTime();
    	for(int i = 0; i < READS; i ++) {
    		list2.get(list1.size() / 2);
    	}
    	
    	time = System.nanoTime() - time;
    	System.out.println("Reading " + READS
                + " times the middle element of list2 " + time
                + "ns (" + time / TO_MS + "ms)");
    	
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
    	
    	final Map<String,Long> map = new HashMap<>();
    	
    	map.put("Africa", AFRICA);
    	map.put("Americas", AMERICAS);
    	map.put("Antarctica", ANTARCTICA);
    	map.put("Asia", ASIA);
    	map.put("Europe", EUROPE);
    	map.put("Oceania", OCEANIA);
    	
        /*
         * 8) Compute the population of the world
         */
    	
    	long totale = 0;
    	for(final long pop : map.values()) {
    		totale += pop;
    	}
    	
    	System.out.println("Popolazione nel mondo: " +totale);
    }
}
