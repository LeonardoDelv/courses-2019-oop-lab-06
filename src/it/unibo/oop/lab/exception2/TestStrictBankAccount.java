package it.unibo.oop.lab.exception2;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {
	
	private static final int INITIAL_AMOUNT = 10_000; 
	
	
    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	
    	AccountHolder a1 = new AccountHolder("Leonardo", "Delvecchio", 1);
    	AccountHolder a2 = new AccountHolder("Leonardo", "Vincenzi", 2);
    	
    	StrictBankAccount leo = new StrictBankAccount(a1.getUserID(), INITIAL_AMOUNT, 10);
    	StrictBankAccount leo2 = new StrictBankAccount(a2.getUserID(), INITIAL_AMOUNT, 10);
    	
    	/*try {
            leo.deposit(5, 100);
            fail();
        } catch (WrongAccountHolderException e) {
            assertNotNull(e);
        }*/
    	
    	/*for(int i=0; i<10; i ++) {
    		try {
    			leo2.depositFromATM(a2.getUserID(), 10);
    		} catch (TransactionsOverQuotaException | WrongAccountHolderException e){
    			fail("Not exceeded yet max n° transactions!");
    		}
    	}
    	
    	try {
            leo2.depositFromATM(a2.getUserID(), 1);
            fail("Should raise the exception signaling that we exceeded max no. transactions!");
        } catch (TransactionsOverQuotaException | WrongAccountHolderException e) {
            assertNotNull(e);
        }*/
    	
    	try {
            /*
             * Questa istruzione genererà una eccezione di tipo
             * NotEnoughFoundsException
             */
            leo.withdraw(a1.getUserID(), 5000000);
        } catch (WrongAccountHolderException e) {
            fail();
        } catch (NotEnoughFoundsException e) {
            assertNotNull(e);
        }
    	
    }
}
