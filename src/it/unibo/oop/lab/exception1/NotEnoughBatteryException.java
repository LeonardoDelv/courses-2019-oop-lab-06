package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends IllegalStateException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final double batteryLevel;
	private final double batteryRequired;
	
	
	public NotEnoughBatteryException(final double batteryLevel, final double batteryRequired) {
		super();
		this.batteryLevel = batteryLevel;
		this.batteryRequired = batteryRequired;
	}


	@Override
	public String toString() {
		return "NotEnoughBatteryException [batteryLevel=" + batteryLevel + ", batteryRequired=" + batteryRequired + "]";
	}
	
	public String getMessage() {
		return this.toString();
	}
}
